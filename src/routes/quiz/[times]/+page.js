import { supabase } from "$lib/supabaseClient";
export const ssr = false

/**@type {import('./$types').PageLoad} */
export async function load({ data }) {

    console.log('files', data)

    const data_db = await supabase.from("countries")
        .select()
        .order('id', { ascending: true })

    console.log('data_db', data_db)

    return {
        // special: { a: 123, b: 456 },
        // matter: ["a", "b", "c"],
        countries: data_db.data ?? [],
        files: data.files ?? []
    }
};
