import type { ServerLoad } from '@sveltejs/kit';
//???
import { CookieSession } from 'svelte-kit-cookie-session/core';

export const load: ServerLoad = async ({ locals }) => {
    // for testing the chunked cookies feature we have to build up a huge array, greater than 4096 bytes
    const hugeArray: number[] = [];

    for (let i = 0; i < 10; i += 1) {
        hugeArray.push(i);
    }

    await locals.session.set({
        views: hugeArray,
        thing: [33, 44, 55]
    });
    // await locals.session.destroy();


    return {
        views: [3, 4, 5] //locals.session.data.counter
    };
};