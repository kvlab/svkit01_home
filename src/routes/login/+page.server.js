import bcrypt from 'bcrypt'
import { redirect, fail } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";

/**@type {import('./$types').Actions} */
export const actions = {
    login: async ({ locals, cookies, url, request }) => {
        //Получим данные формы
        const formdata = await request.formData()
        let username = formdata.get("username")
        let password = formdata.get('password')

        //Проверим, есть ли польз-ль
        let user = await supabase
            .from('user')
            .select()
            .eq('username', username)

        if (!user.data.length) {//Такого польз-ля нет
            return fail(400, { is_user: false })
        }

        //проверим пароль
        const checkPassword = await bcrypt.compare(password, user.data[0].passwordHash)

        if (!checkPassword) {//пароль неправильный
            return fail(400, { bad_pwd: true })
        }

        //обновим данные польз-ля
        let token = crypto.randomUUID()
        const upd_user = await supabase
            .from('user')
            .update({ userAuthToken: token, updated_at: new Date() })
            .eq('id', user.data[0].id)

        if (!upd_user.error) {//нет ошибок при update
            await locals.session.set({ user: { id: user.data[0].id, name: user.data[0].username, token: token }, views: 0, data1: [], data2: [] });

            throw redirect(303, '/')
        } else {//update вызвал ошибку

        }
    }
}