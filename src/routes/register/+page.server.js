import bcrypt from 'bcrypt'
import { fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";

/**@type {import('./$types').Actions} */
export const actions = {
    register: async ({ locals, cookies, url, request }) => {

        //получим ID роли USER
        let { data, error } = await supabase
            .from('roles')
            .select('id')
            .eq('name', 'USER')

        let user_role_id = data[0].id

        //Получим данные формы
        const formdata = await request.formData()
        let username = formdata.get("username")
        let password = formdata.get('password')

        //Проверим, нет ли уже такого польз-ля
        let user = await supabase
            .from('user')
            .select('username')
            .eq('username', username)


        if (user.data.length) {//такой польз-ль уже есть
            return fail(409, { user: true })
        }

        //Создадим польз-ля с ролью USER
        const new_user = await supabase
            .from('user')
            .insert(
                {
                    username: username,
                    passwordHash: await bcrypt.hash(password, 10),
                    userAuthToken: crypto.randomUUID(),
                    fk_roles: user_role_id
                }
            )
        throw redirect(303, '/login')

    }

}