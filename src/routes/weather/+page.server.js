import { fail, redirect } from '@sveltejs/kit';

/**@type {import('./$types').PageServerLoad} */
export async function load({ locals }) {
    if (locals.stop == true) {
        throw redirect(303, '/login')
    }

};