import { fail, redirect } from '@sveltejs/kit';

import * as fs from 'fs'
import { Buffer } from 'buffer';

import { supabase } from "$lib/supabaseClient";

//import { env } from '$env/dynamic/public' //test with /private!!!

/**@type {import('./$types').PageServerLoad} */
export async function load({ locals }) {
    if (locals.stop == true) {
        throw redirect(303, '/login')
    }

};

/**@type {import('./$types').Actions} */
export const actions = {
    add_image: async ({ locals, cookies, url, request }) => {
        const fdata = await request.formData()

        let b64 = fdata.get("b64")
        let txt = fdata.get('txt')
        let offile = fdata.get("offile")
        console.log("offile-", offile, offile.name)


        const { data, error } = await supabase
            .from('images')
            .insert([
                { txt: txt, b64: b64 },
            ])
            .select()


        if (error) {
            console.log(error)
            return fail(parseInt(error.code), { error: error.message });
        }

        //fs.writeFile

        if (offile) {
            const filedata = new Uint8Array(Buffer.from(await offile.arrayBuffer()));
            fs.writeFile(`static/${offile.name}`, filedata, (err) => {
                if (err) {
                    return fail(500, { error: err.message });
                }
                console.log('Файл сохранен');
            });
        }
    }
}

