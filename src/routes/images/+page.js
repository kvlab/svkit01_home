import { supabase } from "$lib/supabaseClient";

/**@type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    const { data } = await supabase.from("images")
        .select()
        .order('id', { ascending: true })


    return {

        images: data ?? [],
    }
};
