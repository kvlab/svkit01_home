import { supabase } from "$lib/supabaseClient";
import { json } from '@sveltejs/kit';

/**   @type {import('./$types').RequestHandler} */
export async function GET({ url }) {

    const id = url.searchParams.get('id')

    const { data } = await supabase.from("countries")
        .select()
        .gte('id', id)
        .order('id', { ascending: true })

    // return new Response(JSON.stringify(data), {
    //     headers: {
    //         'Content-Type': 'application/json'
    //     }
    // });

    // но можно и так
    return json(data)
}

