import { handleSession } from 'svelte-kit-cookie-session';
// import { supabase } from "$lib/supabaseClient";


/** @type {import('@sveltejs/kit').Handle} */
export const handle = handleSession(
    {
        secret: '11111111111111111111111111111111',
        key: "session_cookie",
        expires: 10,
        cookie: {
            httpOnly: true, path: "/"
        }

    },
    ({ event, resolve }) => {
        // event.locals is populated with the session `event.locals.session`
        // Do anything you want here

        console.log("hook ", event.locals.session.data)

        //проверим, есть ли какие-нибудь данные сессии
        if (Object.keys(event.locals.session.data).length) {
            event.locals.stop = false
        } else {
            event.locals.stop = true
        }


        return resolve(event);
    }
);

